﻿
using GalaSoft.MvvmLight.Command;

using Reglas.ViewModels;
using Reglas.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System.Linq;

namespace Reglas.ViewModels
{
    public class RulesViewModel : BaseViewModel
    {
        #region Constructors
        public List<Contrato> ContratosList
        {
            get;
            set;
        }
        public RulesViewModel()

        {
            ContratosList = GetContratos().OrderBy(t => t.Value).ToList();
            this.IsEnabled = true;
        }
        public List<Contrato> GetContratos()
        {
            var contratos = new List<Contrato>()
            {
                new Contrato(){Key = 1, Value="Contrato 1"},
                 new Contrato(){Key = 2, Value="Contrato 2"},
                  new Contrato(){Key = 3, Value="Contrato 3"},
                   new Contrato(){Key = 4, Value="Contrato 4"},
                    new Contrato(){Key = 5, Value="Contrato 5"},
                     new Contrato(){Key = 6, Value="Contrato 6"},
            };
            return contratos;
        }
        public class Contrato
        {
            public int Key
            {
                get;
                set;
            }
            public string Value
            {
                get;
                set;
            }
        }
        #endregion

        #region Attributes

        private bool isEnabled;
        #endregion

        #region Properties
        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }
        #endregion


        #region Commands
        public ICommand ReglaCommand
        {
            get
            {
                return new RelayCommand(Regla);
            }

        }

        private async void Regla()
        {
            MainViewModel.GetInstance().TBLRULES = new TBL_RULESViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new TBL_RULESPage());
            this.IsEnabled = true;

        }
        public ICommand SelectUserCommand
        {
            get
            {
                return new RelayCommand(SelectUser);
            }

        }

        private async void SelectUser()
        {
            MainViewModel.GetInstance().TBLUSERS = new TBL_USERSViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new TBL_USERSPage());
            this.IsEnabled = true;
        }
        public ICommand UserMatchCommand
        {
            get
            {
                return new RelayCommand(UserMatch);
            }

        }

        private async void UserMatch()
        {
            MainViewModel.GetInstance().UserMatch = new UserMatchViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new ScudoTabbedPage());
            this.IsEnabled = true;
        }
        public ICommand SelectClientCommand
        {
            get
            {
                return new RelayCommand(SelectClient);
            }

        }

        private async void SelectClient()
        {
            MainViewModel.GetInstance().TBLCLIENTS = new ContratistaViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new ContratistaPage());
        }
        #endregion
    }
}