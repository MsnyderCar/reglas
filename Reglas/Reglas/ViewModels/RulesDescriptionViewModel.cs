﻿namespace Reglas.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Reglas.Views;
    using System.Windows.Input;
    using Xamarin.Forms;
    using System.Collections.Generic;
    using System.Linq;

    public class RulesDescriptionViewModel : TBL_RULES
    {
        
        

        #region Commands
        public ICommand SelectReglaCommand
        {
            get
            {
                return new RelayCommand(SelectRegla);
            }
            
        }

        private async void SelectRegla()
        {
            MainViewModel.GetInstance().Descripcion = new RuleViewModel(this);
            await Application.Current.MainPage.Navigation.PushAsync(new RulePage());
        }
        #endregion
    }
}
