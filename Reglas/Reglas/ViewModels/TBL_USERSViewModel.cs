﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Reglas.Services;
using Xamarin.Forms;
using Reglas.Models;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Linq;
using Reglas.Views;

namespace Reglas.ViewModels
{
    public class TBL_USERSViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private ObservableCollection<TBL_USERS> tBL_USERs;
        private bool isRefreshing;
        //private string filter;
        private List<TBL_USERS> tBL_USERSList;
        #endregion

        #region Properties
        public ObservableCollection<TBL_USERS> TBL_USERs
        {
            get { return this.tBL_USERs; }
            set { SetValue(ref this.tBL_USERs, value); }
        }
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }
        /*public string Filter
        {
            get { return this.filter; }
            set
            {
                SetValue(ref this.filter, value);
                this.Search();
            }
        }*/
        #endregion

        #region Constructors
        public TBL_USERSViewModel()
        {
            this.apiService = new ApiService();
            this.LoadTBL_USERS();

        }
        #endregion

        #region Methods
        private async void LoadTBL_USERS()
        {

            //this.isRefreshing = true;
            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                this.isRefreshing = false;
                    await Application.Current.MainPage.DisplayAlert(
                   "Error",
                   connection.Message,
                   "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            //http://192.168.0.13:45455/Help/Api/GET-api-TBL_RULES
            
            var response = await this.apiService.GetList<TBL_USERS>("http://api.scudo.com.wm5.my-hosting-panel.com",
               // "https://webapiscudo20190124123906.azurewebsites.net/",
                "/Api",
                "/TBL_USERS");
            this.isRefreshing = false;
            if (!response.IsSuccess)
            {
                this.isRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                "Error",
                response.Message,
                "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            this.tBL_USERSList = (List<TBL_USERS>)response.Result;
            this.TBL_USERs = new ObservableCollection<TBL_USERS>(this.tBL_USERSList);
            //this.tBL_USERSList = (List<TBL_USERS>)response.Result;
            //this.TBL_USERs = new ObservableCollection<TBL_USERS>(
            //this.ToUserViewModel());
            this.isRefreshing = false;
        }

        /* #region Methods
         private IEnumerable<RulesDescriptionViewModel> ToUserViewModel()
         {
             return this.tBL_USERSList.Select(r => new RulesDescriptionViewModel
             {
                 FFTYP_Id = r.FFTYP_Id,
                 FFTYP_TYP_Id = r.FFTYP_TYP_Id,
                 FTYP_FOR_Id = r.FTYP_FOR_Id,
                 FTYP_Position = r.FTYP_Position,
                 FTYP_Days = r.FTYP_Days,
                 FTYP_Active = r.FTYP_Active,
                 FTYP_Deleted = r.FTYP_Deleted,

             });
         } 
         #endregion
*/
        #endregion

        #region Commands
        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadTBL_USERS);
            }
        }
        /*public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }
        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                this.TBL_USERs = new ObservableCollection<TBL_RULES>;
            }
            /*else
            {
                this.TBL_RULEs = new ObservableCollection<TBL_RULES>(
                    this.ToRuleViewModel().Where(r=>
                        r.FFTYP_Id().Contains(this.Filter)));
            }
        }*/

        /*public ICommand SelectUserCommand
        {
            get
            {
                return new RelayCommand(SelectUser);
            }

        }

        private async void SelectUser()
        {
            MainViewModel.GetInstance().TBLRULES = new TBL_RULESViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new TBL_RULESPage());
        }*/
        #endregion



    } }

