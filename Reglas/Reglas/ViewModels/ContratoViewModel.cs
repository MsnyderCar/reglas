﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reglas.ViewModels
{
    public class ContratoViewModel 
    {
        public List<Contrato> ContratosList
        {
            get;
            set;
        }
        public ContratoViewModel()
        {
            ContratosList = GetContratos().OrderBy(t => t.Value).ToList();
        }
        public List<Contrato> GetContratos()
        {
            var contratos = new List<Contrato>()
            {
                new Contrato(){Key = 1, Value="Contrato 1"},
                 new Contrato(){Key = 2, Value="Contrato 2"},
                  new Contrato(){Key = 3, Value="Contrato 3"},
                   new Contrato(){Key = 4, Value="Contrato 4"},
                    new Contrato(){Key = 5, Value="Contrato 5"},
                     new Contrato(){Key = 6, Value="Contrato 6"},
            };
            return contratos;
        }
        public class Contrato
        {
            public int Key
            {
                get;
                set;
            }
            public string Value
            {
                get;
                set;
            }
        }
    }
}
