﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reglas.ViewModels
{
    public class MainViewModel
    {
        #region ViewModels

        public LoginViewModel Login
        {
            get;
            set;
        }
        public RulesViewModel Regla
        {
            get;
            set;
        }

        public TBL_RULESViewModel TBLRULES
        {
            get;
            set;
        }
        public RuleViewModel Descripcion
        {
            get;
            set;
        }
        public TBL_USERSViewModel TBLUSERS
        {
            get;
            set;
        }
        public UserMatchViewModel UserMatch
        {
            get;
            set;
        }
        public ContratistaViewModel TBLCLIENTS
        {
            get;
            set;
        }


        #endregion
        #region Constructors
        public MainViewModel()
        {
            instance = this; 
            this.Regla = new RulesViewModel();
            this.Login = new LoginViewModel();
        }
        #endregion
        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
    }
}
