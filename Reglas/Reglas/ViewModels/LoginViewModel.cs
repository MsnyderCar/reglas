﻿using GalaSoft.MvvmLight.Command;
using Reglas.Models;
using Reglas.Services;
using Reglas.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;


namespace Reglas.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private ObservableCollection<TBL_USERS> tBL_USERs;
        private bool isEnabled;
        private string usuario;
        private string password;

        private List<TBL_USERS> tBL_USERsList;
        private List<TBL_USERS> Listra;
        ListStringTypeConverter getlist;
        private List<TBL_USERS> tBL_USERSList;

        #endregion

        #region Properties
        public ObservableCollection<TBL_USERS> TBL_USERs
        {
            get { return this.tBL_USERs; }
            set { SetValue(ref this.tBL_USERs, value); }
        }
        public string Usuario
        {
            get { return this.usuario; }
            set { SetValue(ref this.usuario, value); }
        }

        public string Password
        {
            get { return this.password; }
            set { SetValue(ref this.password, value); }
        }


        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }
        #endregion
       
        #region Constructors
        public LoginViewModel()
        {
            this.IsEnabled = true;
            this.apiService = new ApiService();
            this.LoadTBL_USERS();
            
            
        }
        

        //this.getUsuario = new Usuario();



        #region Methods
        private async void LoadTBL_USERS()
        {

            //this.isRefreshing = true;
            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert(
               "Error",
               connection.Message,
               "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            //http://192.168.0.13:45455/Help/Api/GET-api-TBL_RULES

            var response = await this.apiService.GetList<TBL_USERS>("http://api.scudo.com.wm5.my-hosting-panel.com",
               //"https://webapiscudo20190124123906.azurewebsites.net/",
                "/Api",
                "/TBL_USERS");

            if (!response.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert(
                "Error",
                response.Message,
                "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            this.tBL_USERSList = (List<TBL_USERS>)response.Result;
            this.TBL_USERs = new ObservableCollection<TBL_USERS>(this.tBL_USERSList);

            
        }

        #endregion

        #endregion

        #region Commands
        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }

        }
       
        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Usuario))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Ingrese Usuario",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Ingrese Contrasena",
                    "Accept");
                return;
            }
            {
                MainViewModel.GetInstance().TBLCLIENTS = new ContratistaViewModel();
                await Application.Current.MainPage.Navigation.PushAsync(new ContratistaPage());
            }
            #endregion
        }
    }

    internal interface IGetTBL_USERs
    {
    }
}






