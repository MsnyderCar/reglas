﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Reglas.Services;
using Xamarin.Forms;
using Reglas.Models;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace Reglas.ViewModels
{
    public class TBL_RULESViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        private DataService dataService;
        #endregion

        #region Attributes
        private ObservableCollection<RulesDescriptionViewModel> tBL_RULEs;
        private bool isRefreshing;
        private string filter;
        private List<TBL_RULES> tBL_RULESList;
        
        #endregion

        #region Properties
        public ObservableCollection<RulesDescriptionViewModel> TBL_RULEs
        {
            get { return this.tBL_RULEs; }
            set { SetValue(ref this.tBL_RULEs, value); }
        }
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }
        public string Filter
        {
            get { return this.filter; }
            set
            {
                SetValue(ref this.filter, value);
                this.Search();
            }
        }
        #endregion

        #region Constructors
        public TBL_RULESViewModel()
        {
            this.apiService = new ApiService();
            this.LoadTBL_RULES();
            this.dataService = new DataService();

        }
        #endregion

        #region Methods
        private async void LoadTBL_RULES()
        {

            this.isRefreshing = true;
            var connection = await this.apiService.CheckConnection();

            if (connection.IsSuccess)
            {
                var answer = await this.LoadTBL_RULESFromAPI();
                if (answer)
                {
                    this.SaveTBL_RULESToDB();
                }
            }
            else
            {
                await this.LoadTBL_RULESFromDB();
            }
            var response = await this.apiService.GetList<TBL_RULES>(

                "http://192.168.0.15:45455/",
                "/Api",
                "/TBL_RULES");
            if (this.tBL_RULESList == null || this.tBL_RULESList.Count == 0)
            {
                this.isRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                "Error",
                response.Message,
                "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
            }
            
            this.TBL_RULEs = new ObservableCollection<RulesDescriptionViewModel>(
                this.ToRuleViewModel());
            this.isRefreshing = false;
        }

        private async Task LoadTBL_RULESFromDB()
        {
            this.tBL_RULESList = await this.dataService.GetAllProducts();
        }

        private async void SaveTBL_RULESToDB()
        {
            await this.dataService.DeleteAllProducts();
            this.dataService.Insert(this.tBL_RULESList);
        }

        private async Task<bool> LoadTBL_RULESFromAPI()
        {

            //"https://webapiscudo20190124123906.azurewebsites.net/",

            this.isRefreshing = true;
            var response = await this.apiService.GetList<TBL_RULES>(

                "http://api.scudo.com.wm5.my-hosting-panel.com",
                "/Api",
                "/TBL_RULES");

            if (!response.IsSuccess)
            {
                return false;
                
            }
            this.tBL_RULESList = (List<TBL_RULES>)response.Result;
            return true;
        }

        #region Methods
        private IEnumerable<RulesDescriptionViewModel> ToRuleViewModel()
        {
            return this.tBL_RULESList.Select(r => new RulesDescriptionViewModel
            {
                FFTYP_Id = r.FFTYP_Id,
                FFTYP_TYP_Id = r.FFTYP_TYP_Id,
                FTYP_FOR_Id = r.FTYP_FOR_Id,
                FTYP_Position = r.FTYP_Position,
                FTYP_Days = r.FTYP_Days,
                FTYP_Active = r.FTYP_Active,
                FTYP_Deleted = r.FTYP_Deleted,
                
            });
        } 
        #endregion

        #endregion

        #region Commands
        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadTBL_RULES);
            }
        }
        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }
        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                this.TBL_RULEs = new ObservableCollection<RulesDescriptionViewModel>(
                this.ToRuleViewModel());
            }
            /*else
            {
                this.TBL_RULEs = new ObservableCollection<TBL_RULES>(
                    this.ToRuleViewModel().Where(r =>
                        r.FFTYP_Id.(this.Filter.ToLower())).OrderBy(r => r.FFTYP_Id));
            }*/
        }
        #endregion



    }
}
