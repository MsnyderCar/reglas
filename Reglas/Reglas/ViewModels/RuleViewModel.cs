﻿namespace Reglas.ViewModels
{
    using Reglas.Models;
    public class RuleViewModel
    {
        #region Properties
        public TBL_RULES TBL_RULES
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public RuleViewModel(TBL_RULES tBL_RULES)
        {
            this.TBL_RULES = tBL_RULES;
        } 
        #endregion

    }
}
