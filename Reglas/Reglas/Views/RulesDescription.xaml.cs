﻿using Reglas.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Reglas.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RulesDescription : ContentPage
	{
		public RulesDescription ()
		{
			InitializeComponent ();
            BindingContext = new RulesDescriptionViewModel();
		}
	}
}