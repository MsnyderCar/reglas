﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reglas.Views;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Reglas.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActionPage : ContentPage
    {
        public ActionPage()
        {
            InitializeComponent();
            Descargar.Tapped += Descargar_Clicked;
        }
        private void Descargar_Clicked(object sender, EventArgs e)
        {
            ((NavigationPage)this.Parent).PushAsync(new ReglasPage());
        }
    }
}