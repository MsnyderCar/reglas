﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reglas.Views;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Reglas.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            btn.Tapped += Btn_Clicked;
        }

        private void Btn_Clicked(object sender, EventArgs e)
        {
            ((NavigationPage)this.Parent).PushAsync(new ActionPage());
        }

    }
}