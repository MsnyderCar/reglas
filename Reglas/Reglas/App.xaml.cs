﻿


namespace Reglas
{
    using System;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;
    using Reglas.Views;
    using Reglas.Models;
    using Reglas.ViewModels;

    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class App : Application
    {
        public NavigationPage LoginPage { get; }
        public NavigationPage ReglasPage { get; }
        public NavigationPage ActionPage { get; }
        public NavigationPage ScudoTabbedPage { get; }

        #region Constructors
        public App()
        {
            InitializeComponent();

            this.MainPage = new NavigationPage (new LoginPage());
            this.ActionPage = new NavigationPage(new ActionPage());
            this.ReglasPage = new NavigationPage(new ReglasPage());
            ReglasPage.BindingContext = new RulesDescriptionViewModel();

        }

        
        #endregion

        #region Methods
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        } 
        #endregion
    }
}
