﻿namespace Reglas.Models
{
    using Newtonsoft.Json;
    using SQLite;

    public class TBL_CLIENTS
    {
       
        [JsonProperty(PropertyName = "COM_Id")]
        public int COM_Id { get; set; }

        [JsonProperty(PropertyName = "COM_TYPCOM_Id")]
        public int COM_TYPCOM_Id { get; set; }

        [JsonProperty(PropertyName = "COM_SEC_Id")]
        public int COM_SEC_Id { get; set; }

        [JsonProperty(PropertyName = "COM_NIT")]
        public int COM_NIT { get; set; }

        [JsonProperty(PropertyName = "COM_Name")]
        public string COM_Name { get; set; }

        [JsonProperty(PropertyName = "FTYP_Days")]
        public int FTYP_Days { get; set; }

        [JsonProperty(PropertyName = "FTYP_Active")]
        public bool FTYP_Active { get; set; }

        [JsonProperty(PropertyName = "FTYP_Deleted")]
        public bool FTYP_Deleted { get; set; }
    }
}
