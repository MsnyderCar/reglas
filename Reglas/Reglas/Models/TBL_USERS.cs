﻿namespace Reglas.Models
{
    using Newtonsoft.Json;
    using SQLite;

    public class TBL_USERS
    {
       
        [JsonProperty(PropertyName = "USR_Id")]
        public int USR_Id { get; set; }

        [JsonProperty(PropertyName = "USR_ROL_Id")]
        public int USR_ROL_Id { get; set; }

        [JsonProperty(PropertyName = "USR_COM_Id")]
        public int USR_COM_Id { get; set; }
        /*[JsonProperty(PropertyName = "FTYP_FOR_Id")]
        public int FTYP_FOR_Id { get; set; }

        [JsonProperty(PropertyName = "FTYP_Position")]
        public int FTYP_Position { get; set; }

        [JsonProperty(PropertyName = "FTYP_Days")]
        public int FTYP_Days { get; set; }

        [JsonProperty(PropertyName = "FTYP_Active")]
        public bool FTYP_Active { get; set; }

        [JsonProperty(PropertyName = "FTYP_Deleted")]
        public bool FTYP_Deleted { get; set; }*/
    }
}
