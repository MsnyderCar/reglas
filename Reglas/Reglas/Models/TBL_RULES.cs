﻿namespace Reglas.Models
{
    using Newtonsoft.Json;
    using SQLite;

    public class TBL_RULES
    {
        [PrimaryKey]
        [JsonProperty(PropertyName = "FTYP_Id")]
        public int FFTYP_Id { get; set; }

        [JsonProperty(PropertyName = "FTYP_TYP_Id")]
        public int FFTYP_TYP_Id { get; set; }

        [JsonProperty(PropertyName = "FTYP_FOR_Id")]
        public int FTYP_FOR_Id { get; set; }

        [JsonProperty(PropertyName = "FTYP_Position")]
        public int FTYP_Position { get; set; }

        [JsonProperty(PropertyName = "FTYP_Days")]
        public int FTYP_Days { get; set; }

        [JsonProperty(PropertyName = "FTYP_Active")]
        public bool FTYP_Active { get; set; }

        [JsonProperty(PropertyName = "FTYP_Deleted")]
        public bool FTYP_Deleted { get; set; }
    }
}