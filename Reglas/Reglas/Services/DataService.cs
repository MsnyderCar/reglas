﻿

namespace Reglas.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
 
    using Interfaces;
    using Reglas.Models;
    using SQLite;
    using Xamarin.Forms;

    public class DataService
    {
        private SQLiteAsyncConnection connection;

        public DataService()
        {
            this.OpenOrCreateDB();
        }

        private async Task OpenOrCreateDB()
        {
            var databasePath = DependencyService.Get<IPathService>().GetDatabasePath();
            this.connection = new SQLiteAsyncConnection(databasePath);
            await connection.CreateTableAsync<TBL_RULES>().ConfigureAwait(false);
        }

        public async Task Insert<T>(T model)
        {
            await this.connection.InsertAsync(model);
        }

        public async Task Insert<T>(List<T> models)
        {
            await this.connection.InsertAllAsync(models);
        }

        public async Task Update<T>(T model)
        {
            await this.connection.UpdateAsync(model);
        }

        public async Task Update<T>(List<T> models)
        {
            await this.connection.UpdateAllAsync(models);
        }

        public async Task Delete<T>(T model)
        {
            await this.connection.DeleteAsync(model);
        }

        public async Task<List<TBL_RULES>> GetAllProducts()
        {
            var query = await this.connection.QueryAsync<TBL_RULES>("select * from [TBL_RULES]");
            var array = query.ToArray();
            var list = array.Select(p => new TBL_RULES
            {
                FFTYP_Id = p.FFTYP_Id,
                FFTYP_TYP_Id = p.FFTYP_TYP_Id,
                FTYP_FOR_Id = p.FTYP_FOR_Id,
                FTYP_Position = p.FTYP_Position,
                FTYP_Days = p.FTYP_Days,
                FTYP_Active = p.FTYP_Active,
                FTYP_Deleted = p.FTYP_Deleted,
            }).ToList();
            return list;
        }

        public async Task DeleteAllProducts()
        {
            var query = await this.connection.QueryAsync<TBL_RULES>("delete from [TBL_RULES]");
        }
    }

}
