﻿namespace Reglas.Interfaces
{
    public interface IPathService
    {
        string GetDatabasePath();
    }
}
