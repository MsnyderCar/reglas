﻿[assembly: Xamarin.Forms.Dependency(typeof(Reglas.Droid.Implementations.PathService))]

namespace Reglas.Droid.Implementations
{
    using Interfaces;
    using System;
    using System.IO;

    public class PathService : IPathService
    {
        public string GetDatabasePath()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, "Reglas.db3");
        }
    }
}
